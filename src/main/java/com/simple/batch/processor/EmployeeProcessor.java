package com.simple.batch.processor;

import com.simple.batch.model.Associate;
import com.simple.batch.model.Employee;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by szagoret on 2/25/2016.
 */
public class EmployeeProcessor implements ItemProcessor<Employee, Associate> {

    @Override
    public Associate process(Employee employee) throws Exception {
        return new Associate(employee);
    }
}
