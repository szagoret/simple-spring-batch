package com.simple.batch.listener;

import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.listener.ChunkListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by szagoret on 2/25/2016.
 */
public class EmployeeChunkListener extends ChunkListenerSupport {


    @Autowired
    private JobOperator simpleJobOperator;

    @Override
    public void afterChunk(ChunkContext context) {
        System.out.println("Write : " + context.getStepContext().getStepExecution().getWriteCount() + " rows");
    }
}
