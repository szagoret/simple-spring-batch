package com.simple.batch.listener;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * Created by szagoret on 2/24/2016.
 */
public class JobStatusListener implements JobExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("Job: " + jobExecution.getJobInstance().getJobName() + " is beginning");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("Job: " + jobExecution.getJobInstance().getJobName() + " has completed");
        System.out.println("The Job Execution status is: " + jobExecution.getStatus());
        System.out.println("Total execution seconds: " +
                (jobExecution.getEndTime().getTime() - jobExecution.getStartTime().getTime()) / 1000);
    }
}
