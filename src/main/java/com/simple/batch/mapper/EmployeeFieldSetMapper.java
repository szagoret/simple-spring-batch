package com.simple.batch.mapper;

import com.simple.batch.model.Employee;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

/**
 * Created by szagoret on 2/24/2016.
 */
public class EmployeeFieldSetMapper implements FieldSetMapper<Employee> {
    @Override
    public Employee mapFieldSet(FieldSet fieldSet) throws BindException {
        Employee employee = new Employee();
        employee.setId(fieldSet.readInt("ID"));
        employee.setLastName(fieldSet.readString("lastName"));
        employee.setFirstName(fieldSet.readString("firstName"));
        employee.setDesignation(fieldSet.readString("designation"));
        employee.setDepartment(fieldSet.readString("department"));
        employee.setYearOfJoining(fieldSet.readInt("yearOfJoining"));
        return employee;
    }
}
