/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simple.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class SampleBatchApplication {

    private JobBuilderFactory jobs;

    private StepBuilderFactory steps;


    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:mongodb-batch.xml");
        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
        Job job = (Job) context.getBean("splitBigFile");
        try {
            Map<String, JobParameter> jobParameterMap = new LinkedHashMap<>();
            JobParameter jobParameter = new JobParameter("employeetosplit.csv", false);
            JobParameter jobParameterTimestamp = new JobParameter(new Date().getTime(), true);
            jobParameterMap.put("employeeFile", jobParameter);
            jobParameterMap.put("timestamp", jobParameterTimestamp);
            JobParameters jobParameters = new JobParameters(jobParameterMap);
            JobExecution jobExecution = jobLauncher.run(job, jobParameters);
            System.out.println("Exit status: " + jobExecution.getStatus());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        System.out.println("Job Done");
    }

    private void initBeans(ApplicationContext context) {

    }
}
