package com.simple.batch.preparedstatement;

import com.simple.batch.model.Employee;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by szagoret on 2/25/2016.
 */
public class EmployeePreparedStatementSetter implements ItemPreparedStatementSetter<Employee> {

    @Override
    public void setValues(Employee item, PreparedStatement ps) throws SQLException {
        ps.setInt(1, item.getId());
        ps.setString(2, item.getLastName());
        ps.setString(3, item.getFirstName());
        ps.setString(4, item.getDesignation());
        ps.setString(5, item.getDepartment());
        ps.setInt(6, item.getYearOfJoining());
    }
}
