package com.simple.batch.model;

/**
 * Created by szagoret on 2/25/2016.
 */
public class Associate {

    private Employee employee;

    public Associate(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
